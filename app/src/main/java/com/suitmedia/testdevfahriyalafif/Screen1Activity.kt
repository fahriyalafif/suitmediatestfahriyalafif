package com.suitmedia.testdevfahriyalafif

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.github.dhaval2404.imagepicker.ImagePicker
import com.suitmedia.testdevfahriyalafif.databinding.ActivityScreen1Binding
import com.suitmedia.testdevfahriyalafif.extension.toast
import com.suitmedia.testdevfahriyalafif.util.User
import java.io.File


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class Screen1Activity : AppCompatActivity() {

    private var _binding: ActivityScreen1Binding? = null
    private val binding get() = _binding!!

    private var selectedFileUri: Uri? = null
    private val user: User = User.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityScreen1Binding.inflate(layoutInflater)

        setContentView(binding.root)

        supportActionBar?.hide()
        binding.btCheck.setOnClickListener {
            if(binding.eTPalindrome.text.isNullOrBlank()) {
                toast("Please type palindrome text")
                binding.eTPalindrome.error = "Please type palindrome text"
            }
            else if(isPalindrome(binding.eTPalindrome.text.toString())){
                toast("Text is Palindrome")
                binding.eTPalindrome.error = null
            }
            else {
                toast("Text is not Palindrome")
                binding.eTPalindrome.error = "Text is not Palindrome"
            }
        }

        binding.imgAvatar.setOnClickListener{
            val permissionWriteStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
            if (permissionGranted(permissionWriteStorage)) startImagePicker()
            else permissionLauncher.launch(permissionWriteStorage)
        }

        binding.eTName.setText(user.name)
        if(user.avatar.isNullOrBlank()) binding.imgAvatar.setImageResource(R.drawable.img_avatar)
        else {
            try {
                binding.imgAvatar.setImageURI(Uri.fromFile(File(user.avatar)))
            }
            catch (e: Exception){
                e.printStackTrace()
            }
        }

        binding.btNext.setOnClickListener {
            var valid = true
            if(binding.eTPalindrome.text.isNullOrBlank()) {
                binding.eTPalindrome.error = "Please type palindrome text"
                valid = false
            }
            if(binding.eTName.text.isNullOrBlank()){
                binding.eTName.error = "Please tyoe your name"
                valid = false
            }

            if(valid){
                user.name = binding.eTName.text.toString()
                selectedFileUri?.let {
                    user.avatar = it.path
                }

                //kode untuk membuka halaman screen 2
                startActivity(Intent(this, Screen2Activity::class.java))
                finish()
            }
        }

    }

    private fun startImagePicker(){
        ImagePicker.with(this)
            .cropSquare()
            .createIntent { intent ->
                startForCustomerImageResult.launch(intent)
            }
    }

    private fun isPalindrome(text: String): Boolean {
        val reverseString: String = text.reversed()
        return text.equals(reverseString, ignoreCase = true)
    }

    private val startForCustomerImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                selectedFileUri = result.data?.data
                //val fileImage: File? = selectedFileUri?.path?.let { File(it) }
                binding.imgAvatar.setImageURI(selectedFileUri)
            }
            else if (result.resultCode == ImagePicker.RESULT_ERROR) {
                toast(ImagePicker.getError(result.data))
            }
        }

    private val permissionLauncher: ActivityResultLauncher<String> =
        registerForActivityResult(ActivityResultContracts.RequestPermission()){ isGranted ->
            if(isGranted) startImagePicker()
            else{
                toast("Please enable write storage permission")

                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                val uri = Uri.fromParts("package", packageName, null)
                intent.data = uri
                startActivity(intent)
            }
        }

    fun permissionGranted(permission: String): Boolean {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || ActivityCompat.checkSelfPermission(
            this,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}