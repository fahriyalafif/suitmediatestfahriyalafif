package com.suitmedia.testdevfahriyalafif.util;

import android.content.Context;
import android.content.SharedPreferences;

/*
 * Created by fahriyalafif
 */
public class User {
    public static final String AVATAR = "avatar",NAME = "name", EVENT = "event", GUEST = "guest";

    private static User user;
    private final SharedPreferences pref;

    public static void init(Context context){
        user = new User(context);
    }

    public static User getInstance(){
        return user;
    }

    private User(Context context){
        pref = context.getSharedPreferences(context.getPackageName()+"_user", Context.MODE_PRIVATE);
    }

    private void setData(String k, String v){
        pref.edit().putString(k, v).commit();
    }

    private String getData(String key) {
        return pref.getString(key, "");
    }

    public void setName(String s){
        setData(NAME, s);
    }

    public String getName(){
        return getData(NAME);
    }

    public void setAvatar(String s){
        setData(AVATAR, s);
    }

    public String getAvatar(){
        return getData(AVATAR);
    }

    public void setEvent(String s){
        setData(EVENT, s);
    }

    public String getEvent(){
        return getData(EVENT);
    }

    public void setGuest(String s){
        setData(GUEST, s);
    }

    public String getGuest(){
        return getData(GUEST);
    }

}
