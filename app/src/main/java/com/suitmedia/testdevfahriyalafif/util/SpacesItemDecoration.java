package com.suitmedia.testdevfahriyalafif.util;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by didik on 20/05/16.
 * Item Decor
 */
public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int mItemOffset;

    public SpacesItemDecoration(int itemOffset) {
        mItemOffset = itemOffset;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent,
                               @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        if(parent.getLayoutManager() instanceof GridLayoutManager){
            outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
        }
        else {
            LinearLayoutManager lm = (LinearLayoutManager) parent.getLayoutManager();

            int position = parent.getChildLayoutPosition(view);
            if(lm.getOrientation() == RecyclerView.VERTICAL) {
                outRect.right = mItemOffset;
                outRect.left = mItemOffset;
                outRect.bottom = mItemOffset;
                if (position == 0) outRect.top = mItemOffset;
            }
            else {
                outRect.right = mItemOffset;
                outRect.bottom = mItemOffset;
                outRect.top = mItemOffset;
                if (position == 0) outRect.left = mItemOffset;
            }
        }
    }
}
