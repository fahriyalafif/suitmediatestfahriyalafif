package com.suitmedia.testdevfahriyalafif.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.suitmedia.testdevfahriyalafif.R
import com.suitmedia.testdevfahriyalafif.adapter.EventAdapter
import com.suitmedia.testdevfahriyalafif.databinding.FragmentEventListBinding
import com.suitmedia.testdevfahriyalafif.extension.toast
import com.suitmedia.testdevfahriyalafif.model.Event
import com.suitmedia.testdevfahriyalafif.util.SpacesItemDecoration
import com.suitmedia.testdevfahriyalafif.util.User

class EventListFragment : Fragment() {

    private val user: User = User.getInstance()
    private var _binding: FragmentEventListBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = FragmentEventListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val items = ArrayList<Event>()
        items.add(
            Event(1,
                "Workshop android",
                "Lorem ipsum dolor sit amet bla blab bla bla la",
                R.drawable.gunung_persegi,
                "20 Sept 2022",
                "18:30",
                -7.261438960178676,
                112.75571705291075
            )
        )

        items.add(
            Event(2,
                "Pelatihan memasak",
                "Lorem ipsum dolor sit amet bla blab bla bla la",
                R.drawable.img_1,
                "20 Sept 2022",
                "18:30",
                -7.265312918225506,
                112.77468563414656
            )
        )

        items.add(
            Event(3,
                "Google meet up",
                "Lorem ipsum dolor sit amet bla blab bla bla la",
                R.drawable.img_2,
                "20 Sept 2022",
                "18:30",
                -7.272507323134937,
                112.7588498728886
            )
        )

        items.add(
            Event(4,
                "Nobar Sepak Takraw",
                "Lorem ipsum dolor sit amet bla blab bla bla la",
                R.drawable.img_3,
                "20 Sept 2022",
                "18:30",
                -7.271315358802841,
                112.75249840220702
            )
        )

        items.add(
            Event(5,
                "Seminar Kesehatan Mata",
                "Lorem ipsum dolor sit amet bla blab bla bla la",
                R.drawable.gunung_persegi,
                "20 Sept 2022",
                "18:30",
                -7.251306912013068,
                112.76481510546164
            )
        )

        items.add(
            Event(6,
                "Seminar Website",
                "Lorem ipsum dolor sit amet bla blab bla bla la",
                R.drawable.img_2,
                "20 Sept 2022",
                "18:30",
                -7.333333,
                112.44444
            )
        )

        items.add(
            Event(7,
                "Lomba makan nasi padang",
                "Lorem ipsum dolor sit amet bla blab bla bla la",
                R.drawable.img_4,
                "20 Sept 2022",
                "18:30",
                -7.777777,
                112.222222
            )
        )

        val eventAdapter = EventAdapter(items)
        eventAdapter.setEventClickListener(object : EventAdapter.EventClickListener{
            override fun onClick(event: Event) {
                user.event = event.title
                requireContext().toast("Selected event = ${event.title}")
            }

        })

        binding.recyclerView.apply {
            adapter = eventAdapter
            layoutManager = LinearLayoutManager(requireContext())
            addItemDecoration(SpacesItemDecoration(32))
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}