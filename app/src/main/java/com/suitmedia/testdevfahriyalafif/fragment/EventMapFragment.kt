package com.suitmedia.testdevfahriyalafif.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.suitmedia.testdevfahriyalafif.R
import com.suitmedia.testdevfahriyalafif.databinding.FragmentEventMapBinding
import com.suitmedia.testdevfahriyalafif.model.Event

class EventMapFragment : Fragment(), OnMapReadyCallback {

    private var _binding: FragmentEventMapBinding? = null
    private val binding get() = _binding!!

    private var lastMarkerClicked: Marker? = null

    private var hashMapMarkerEvent = HashMap<String, Event>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = FragmentEventMapBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onMapReady(googleMap: GoogleMap) {

        val items = ArrayList<Event>()
        items.add(
            Event(1,
                "Workshop android",
                "Lorem ipsum dolor sit amet bla blab bla bla la",
                R.drawable.gunung_persegi,
                "20 Sept 2022",
                "18:30",
                -7.261438960178676,
                112.75571705291075
            )
        )

        items.add(
            Event(2,
                "Pelatihan memasak",
                "Lorem ipsum dolor sit amet bla blab bla bla la",
                R.drawable.img_1,
                "20 Sept 2022",
                "18:30",
                -7.265312918225506,
                112.77468563414656
            )
        )

        items.add(
            Event(3,
                "Google meet up",
                "Lorem ipsum dolor sit amet bla blab bla bla la",
                R.drawable.img_2,
                "20 Sept 2022",
                "18:30",
                -7.272507323134937,
                112.7588498728886
            )
        )

        items.add(
            Event(4,
                "Nobar Sepak Takraw",
                "Lorem ipsum dolor sit amet bla blab bla bla la",
                R.drawable.img_3,
                "20 Sept 2022",
                "18:30",
                -7.271315358802841,
                112.75249840220702
            )
        )

        items.add(
            Event(5,
                "Seminar Kesehatan Mata",
                "Lorem ipsum dolor sit amet bla blab bla bla la",
                R.drawable.gunung_persegi,
                "20 Sept 2022",
                "18:30",
                -7.251306912013068,
                112.76481510546164
            )
        )

        items.add(
            Event(6,
                "Seminar Website",
                "Lorem ipsum dolor sit amet bla blab bla bla la",
                R.drawable.img_2,
                "20 Sept 2022",
                "18:30",
                -7.266369142566798,
                112.71454012327719
            )
        )

        items.add(
            Event(7,
                "Lomba makan nasi padang",
                "Lorem ipsum dolor sit amet bla blab bla bla la",
                R.drawable.img_4,
                "20 Sept 2022",
                "18:30",
                -7.188713528263147,
                112.78028642781892
            )
        )

        items.forEach{
            val marker: Marker? = googleMap.addMarker(
                MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_unselected))
                    .position(LatLng(it.latitude, it.longitude))
            )

            if(marker != null) {
                hashMapMarkerEvent[marker.id] = it
            }
        }

        googleMap.uiSettings.apply {
            isZoomControlsEnabled = true
            isZoomGesturesEnabled = true
        }
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(-7.279310444318158, 112.74217760265003), 13f))
        googleMap.setOnMarkerClickListener {

            lastMarkerClicked?.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_unselected))
            it.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_selected))
            lastMarkerClicked = it

            binding.eventDetailView.visibility = View.VISIBLE
            val event = hashMapMarkerEvent[it.id]
            with(binding) {
                if(event != null) {
                    Glide.with(requireContext())
                        .load(event.image)
                        .into(image)

                    title.text = event.title
                    description.text = event.description
                    date.text = event.date
                    time.text = event.time
                }
            }

            return@setOnMarkerClickListener false
        }
    }

}