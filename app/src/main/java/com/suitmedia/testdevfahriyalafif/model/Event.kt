package com.suitmedia.testdevfahriyalafif.model

data class Event(
    val id: Int,
    val title: String,
    val description: String,
    val image: Int,
    val date: String,
    val time: String,
    val latitude: Double,
    val longitude: Double
)