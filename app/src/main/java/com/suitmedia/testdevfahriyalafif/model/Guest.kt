package com.suitmedia.testdevfahriyalafif.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Guest (
    @PrimaryKey val id: Int,
    val email: String,
    val firstName: String,
    val lastName: String,
    val avatar: String
)