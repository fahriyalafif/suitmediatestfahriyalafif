package com.suitmedia.testdevfahriyalafif.model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.suitmedia.testdevfahriyalafif.model.Guest

@Dao
interface GuestDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGuest(guest: Guest)

    @Query("Delete from guest")
    fun deleteAll()

    @Query("Select * from guest")
    fun getAll(): List<Guest>

}