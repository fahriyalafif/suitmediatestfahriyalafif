package com.suitmedia.testdevfahriyalafif

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.suitmedia.testdevfahriyalafif.databinding.ActivityScreen3Binding
import com.suitmedia.testdevfahriyalafif.fragment.EventListFragment
import com.suitmedia.testdevfahriyalafif.fragment.EventMapFragment

class Screen3Activity : AppCompatActivity() {

    private var _binding: ActivityScreen3Binding? = null
    private val binding get() = _binding!!
    private lateinit var menuMap: MenuItem
    private lateinit var menuList: MenuItem

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityScreen3Binding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)
        }

        supportFragmentManager.beginTransaction().apply {
            replace(binding.fragmentContainer.id, EventListFragment())
            commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            menuMap.itemId -> {
                //ganti ke halaman map
                menuMap.isVisible = false
                menuList.isVisible = true

                supportFragmentManager.beginTransaction().apply {
                    replace(binding.fragmentContainer.id, EventMapFragment())
                    commit()
                }

                return true
            }
            menuList.itemId -> {
                //ganti ke halaman list
                menuMap.isVisible = true
                menuList.isVisible = false

                supportFragmentManager.beginTransaction().apply {
                    replace(binding.fragmentContainer.id, EventListFragment())
                    commit()
                }

                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_event, menu)
        menuMap = menu.findItem(R.id.menu_map)
        menuList = menu.findItem(R.id.menu_list)

        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}