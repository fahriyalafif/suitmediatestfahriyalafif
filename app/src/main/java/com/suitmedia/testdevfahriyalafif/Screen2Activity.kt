package com.suitmedia.testdevfahriyalafif

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.suitmedia.testdevfahriyalafif.databinding.ActivityScreen2Binding
import com.suitmedia.testdevfahriyalafif.util.User

class Screen2Activity : AppCompatActivity() {

    private var _binding: ActivityScreen2Binding? = null
    private val binding get() = _binding!!
    private val user: User = User.getInstance()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityScreen2Binding.inflate(layoutInflater)

        setContentView(binding.root)

        supportActionBar?.hide()

        binding.tvUser.text = user.name + "!"

        binding.chooseEvent.setOnClickListener {
            startActivity(Intent(this, Screen3Activity::class.java))
        }

        binding.chooseGuest.setOnClickListener {
            startActivity(Intent(this, Screen4Activity::class.java))
        }

    }

    override fun onResume() {
        super.onResume()
        binding.chooseEvent.text = if(user.event.isNullOrBlank()) "Choose Event" else user.event
        binding.chooseGuest.text = if(user.guest.isNullOrBlank()) "Choose Guest" else user.guest
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}