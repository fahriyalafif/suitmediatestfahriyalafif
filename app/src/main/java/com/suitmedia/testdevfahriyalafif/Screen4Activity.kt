package com.suitmedia.testdevfahriyalafif

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.suitmedia.testdevfahriyalafif.adapter.GuestAdapter
import com.suitmedia.testdevfahriyalafif.database.AppDatabase
import com.suitmedia.testdevfahriyalafif.databinding.ActivityScreen4Binding
import com.suitmedia.testdevfahriyalafif.extension.isOnLine
import com.suitmedia.testdevfahriyalafif.extension.toast
import com.suitmedia.testdevfahriyalafif.model.Guest
import com.suitmedia.testdevfahriyalafif.util.SpacesItemDecoration
import com.suitmedia.testdevfahriyalafif.util.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONObject

class Screen4Activity : AppCompatActivity() {

    private var _binding: ActivityScreen4Binding? = null
    private val binding get() = _binding!!
    private val user = User.getInstance()
    private val guestAdapter = GuestAdapter()
    private var page = 1
    private lateinit var lm: GridLayoutManager
    private var nextPage = true
    private var isLoadingData = false

    companion object{
        const val PER_PAGE = 6
    }

    private lateinit var db: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityScreen4Binding.inflate(layoutInflater)
        setContentView(binding.root)

        db = Room.databaseBuilder(
            this,
            AppDatabase::class.java, "test-dev-database.db"
        ).build()

        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)
        }

        guestAdapter.setGuestClickListener(object : GuestAdapter.GuestClickListener {
            override fun onClick(guest: Guest) {
                user.guest = guest.firstName + " " + guest.lastName
                toast("Selected guest is ${user.guest}")
            }
        })

        lm = GridLayoutManager(this@Screen4Activity, 2)

        binding.recyclerView.apply {
            adapter = guestAdapter
            layoutManager = lm
            addItemDecoration(SpacesItemDecoration(40))
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)

                    val currentItemViewed: Int = lm.childCount
                    val firstItemViewed: Int = lm.findFirstVisibleItemPosition()

                    if (nextPage && guestAdapter.itemCount > 0 && currentItemViewed + firstItemViewed == guestAdapter.itemCount
                        && newState == RecyclerView.SCROLL_STATE_IDLE && !isLoadingData && isOnLine()) {
                        page++
                        loadDataOnline()
                    }
                }
            })
        }

        binding.swipeRefresh.setOnRefreshListener {
            if (isOnLine()) {
                CoroutineScope(Dispatchers.IO).launch {
                    db.guestDao().deleteAll()

                    launch (Dispatchers.Main) {
                        page = 1
                        nextPage = true
                        isLoadingData = false
                        guestAdapter.clearItems()

                        loadDataOnline()
                    }
                }
            }
            else toast("No internet connection")
        }

        CoroutineScope(Dispatchers.IO).launch {
            val listGuestLocal = db.guestDao().getAll()
            launch(Dispatchers.Main) {
                if(listGuestLocal.isEmpty()) {
                    binding.swipeRefresh.isRefreshing = true
                    loadDataOnline()
                }
                else{
                    nextPage = false
                    guestAdapter.addItems(ArrayList(listGuestLocal))
                }
            }
        }

    }

    private fun loadDataOnline(){

        if (isOnLine()) {
            isLoadingData = true

            if (! binding.swipeRefresh.isRefreshing && ! binding.loadMore.isShown) {
                binding.loadMore.visibility = View.VISIBLE
                binding.loadMore.startAnimation(
                    AnimationUtils.loadAnimation(
                        this,
                        R.anim.slide_in_from_bottom
                    )
                )
            }

            AndroidNetworking.get("https://reqres.in/api/users")
                .addQueryParameter("page", page.toString())
                .addQueryParameter("per_page", PER_PAGE.toString())
                .doNotCacheResponse()
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener{
                    override fun onResponse(response: JSONObject?) {
                        try{
                            isLoadingData = false

                            binding.swipeRefresh.isRefreshing = false

                            if (binding.loadMore.isShown) {
                                binding.loadMore.visibility = View.GONE
                                binding.loadMore.startAnimation(
                                    AnimationUtils.loadAnimation(
                                        this@Screen4Activity,
                                        R.anim.slide_out_to_bottom
                                    )
                                )
                            }

                            response?.let {
                                val data = it.getJSONArray("data")

                                nextPage = data.length() > 0

                                if(nextPage){
                                    val newItems = ArrayList<Guest>()
                                    for(i in 0 until data.length()){
                                        val ob = data.getJSONObject(i)
                                        newItems.add(
                                            Guest(
                                                ob.getInt("id"),
                                                ob.getString("email"),
                                                ob.getString("first_name"),
                                                ob.getString("last_name"),
                                                ob.getString("avatar")
                                            )
                                        )
                                    }

                                    CoroutineScope(Dispatchers.IO).launch {
                                        val guestDao = db.guestDao()
                                        newItems.forEach {
                                            guestDao.insertGuest(it)
                                        }

                                        launch (Dispatchers.Main) {
                                            guestAdapter.addItems(newItems)
                                        }
                                    }
                                }
                            }
                        }
                        catch (e: Exception){
                            e.printStackTrace()
                            e.message?.let { toast(it) }
                        }
                    }

                    override fun onError(anError: ANError?) {
                        try {
                            isLoadingData = false
                            binding.swipeRefresh.isRefreshing = false
                            if (binding.loadMore.isShown) {
                                binding.loadMore.visibility = View.GONE
                                binding.loadMore.startAnimation(
                                    AnimationUtils.loadAnimation(
                                        this@Screen4Activity,
                                        R.anim.slide_out_to_bottom
                                    )
                                )
                            }
                            anError?.message?.let {
                                toast(it)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            e.message?.let { toast(it) }
                        }
                    }

                })
        }
        else toast("No internet connection")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        AndroidNetworking.cancelAll()
        _binding = null
    }

}