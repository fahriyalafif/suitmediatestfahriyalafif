package com.suitmedia.testdevfahriyalafif.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.suitmedia.testdevfahriyalafif.model.Guest
import com.suitmedia.testdevfahriyalafif.model.dao.GuestDao

@Database(entities = [Guest::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun guestDao(): GuestDao
}