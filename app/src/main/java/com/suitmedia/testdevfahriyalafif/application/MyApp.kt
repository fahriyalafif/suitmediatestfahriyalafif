package com.suitmedia.testdevfahriyalafif.application

import android.app.Application
import com.androidnetworking.AndroidNetworking
import com.suitmedia.testdevfahriyalafif.util.User

class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()

        User.init(this)

        AndroidNetworking.initialize(this)
        AndroidNetworking.enableLogging()
    }

}