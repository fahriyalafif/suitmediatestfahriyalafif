package com.suitmedia.testdevfahriyalafif.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.suitmedia.testdevfahriyalafif.databinding.GuestItemBinding
import com.suitmedia.testdevfahriyalafif.model.Guest

class GuestAdapter() : RecyclerView.Adapter<GuestAdapter.GuestHolder>() {

    private val items = ArrayList<Guest>()

    interface GuestClickListener{
        fun onClick(guest: Guest)
    }

    private var guestClickListener: GuestClickListener? = null

    fun setGuestClickListener(listener: GuestClickListener){
        this.guestClickListener = listener
    }

    inner class GuestHolder(private val binding: GuestItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bindData(guest: Guest) {
            with(binding){
                Glide.with(root.context)
                    .load(guest.avatar)
                    .into(image)

                fullname.text = guest.firstName + " " + guest.lastName

                root.setOnClickListener{
                    guestClickListener?.onClick(guest)
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GuestHolder =
        GuestHolder(
            GuestItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun onBindViewHolder(holder: GuestHolder, position: Int) {
        holder.bindData(items[position])
    }

    override fun getItemCount(): Int = items.size

    fun clearItems(){
        items.clear()
        notifyDataSetChanged()
    }

    fun addItems(newItems: ArrayList<Guest>){
        val positionStart = items.size
        items.addAll(newItems)
        notifyItemRangeInserted(positionStart, newItems.size)
    }

}