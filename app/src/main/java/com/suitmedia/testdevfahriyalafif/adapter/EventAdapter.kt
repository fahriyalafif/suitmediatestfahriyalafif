package com.suitmedia.testdevfahriyalafif.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.suitmedia.testdevfahriyalafif.databinding.EventItemBinding
import com.suitmedia.testdevfahriyalafif.model.Event

class EventAdapter(private val items: ArrayList<Event>)
    : RecyclerView.Adapter<EventAdapter.EventHolder>() {

    interface EventClickListener{
        fun onClick(event: Event)
    }

    private var eventClickListener: EventClickListener? = null

    fun setEventClickListener(listener: EventClickListener){
        this.eventClickListener = listener
    }

    inner class EventHolder(private val binding: EventItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindData(event: Event) {
            with(binding){
                Glide.with(root.context)
                    .load(event.image)
                    .into(image)

                title.text = event.title
                description.text = event.description
                date.text = event.date
                time.text = event.time

                root.setOnClickListener{
                    eventClickListener?.onClick(event)
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventHolder =
        EventHolder(
            EventItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun onBindViewHolder(holder: EventHolder, position: Int) {
        holder.bindData(items[position])
    }

    override fun getItemCount(): Int = items.size

}